package guy.droid.com.analytics;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class MainActivity extends AppCompatActivity {
    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(100);
        tracker = analytics.newTracker("UA-82067594-1");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tracker.enableExceptionReporting(false);
        tracker.enableAdvertisingIdCollection(false);
        tracker.enableAutoActivityTracking(false);

    }
}
